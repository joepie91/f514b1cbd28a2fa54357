// BAD:

if (condition == true)
	doThing();
	
// GOOD:

if (condition == true) {
	doThing();
}